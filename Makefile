CC=gcc
CFLAGS=-I.
OBJ = main.o mask.o

%.o:
	$(CC) -c mask.c $(CFLAGS)
	$(CC) -c -Wall main.c $(CFLAGS)

all: main.o mask.o
	$(CC) -o $@ $^ $(CFLAGS) 

clean:
	rm -f *.o all
