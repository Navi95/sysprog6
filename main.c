#include "mask.h"
#include <stdio.h>

static int mask_owner, mask_group, mask_other;


int main() 
{
	int v1=1;
	int v2=1;	
	while(v1==1)
	{			
		char resp;
		while(v2==1)
		{
			
			printf("1.Permiso lectura para owner? [s/n]: ");
			scanf(" %c", &resp);
			if(resp == 's')
			{
				setPermiso(&mask_owner,'r', SET);
			}
			else if(resp =='n')
			{
				setPermiso(&mask_owner,'r', UNSET);
			}else
			{
				printf("--ERROR: RESPUESTA ERRONEA--\n");
				break;
				
			}
			
			
			printf("2.Permiso escritura para owner? [s/n]: ");
			scanf(" %c", &resp);
			if(resp == 's')
			{
				setPermiso(&mask_owner,'w', SET);
			}
			else if(resp =='n')
			{
				setPermiso(&mask_owner,'w', UNSET);
			}else
			{
				printf("--ERROR: RESPUESTA ERRONEA--\n");
				break;
				
			}
			
			
			printf("3.Permiso ejecución para owner? [s/n]: ");
			scanf(" %c", &resp);
			if(resp == 's')
			{
				setPermiso(&mask_owner,'x', SET);
			}
			else if(resp =='n')
			{
				setPermiso(&mask_owner,'x', UNSET);
			}else
			{
				printf("--ERROR: RESPUESTA ERRONEA--\n");
				break;
				
			}
			
			
			printf("4.Permiso lectura para group? [s/n]: ");
			scanf(" %c", &resp);
			if(resp == 's')
			{
				setPermiso(&mask_group,'r', SET);
			}
			else if(resp =='n')
			{
				setPermiso(&mask_group,'r', UNSET);
			}else
			{
				printf("--ERROR: RESPUESTA ERRONEA--\n");
				break;
				
			}
			
			
			printf("5.Permiso escritura para group? [s/n]: ");
			scanf(" %c", &resp);
			if(resp == 's')
			{
				setPermiso(&mask_group,'w', SET);
			}
			else if(resp =='n')
			{
				setPermiso(&mask_group,'w', UNSET);
			}else
			{
				printf("--ERROR: RESPUESTA ERRONEA--\n");
				break;
				
			}
			
			
			printf("6.Permiso ejecución para group? [s/n]: ");
			scanf(" %c", &resp);
			if(resp == 's')
			{
				setPermiso(&mask_group,'x', SET);
			}
			else if(resp =='n')
			{
				setPermiso(&mask_group,'x', UNSET);
			}else
			{
				printf("--ERROR: RESPUESTA ERRONEA--\n");
				break;
				
			}
			
			
			printf("7.Permiso lectura para other? [s/n]: ");
			scanf(" %c", &resp);
			if(resp == 's')
			{
				setPermiso(&mask_other,'r', SET);
			}
			else if(resp =='n')
			{
				setPermiso(&mask_other,'r', UNSET);
			}else
			{
				printf("--ERROR: RESPUESTA ERRONEA--\n");
				break;
				
			}
			
			
			printf("8.Permiso escritura para other? [s/n]: ");
			scanf(" %c", &resp);
			if(resp == 's')
			{
				setPermiso(&mask_other,'w', SET);
			}
			else if(resp =='n')
			{
				setPermiso(&mask_other,'w', UNSET);
			}else
			{
				printf("--ERROR: RESPUESTA ERRONEA--\n");
				break;
				
			}
			
			
			printf("9.Permiso ejecución para other? [s/n]: ");
			scanf(" %c", &resp);
			if(resp == 's')
			{
				setPermiso(&mask_other,'x', SET);
			}
			else if(resp =='n')
			{
				setPermiso(&mask_other,'x', UNSET);
			}else
			{
				printf("--ERROR: RESPUESTA ERRONEA--\n");
				break;
				
			}
			
			printf("MASCARA OCTAL RESULTANTE: %d%d%d\n", mask_owner, mask_group, mask_other);
			
			printf("DESEA SALIR? [s/n]: ");
			scanf(" %c", &resp);
			if(resp == 's')
			{
				v1=0;
				break;
			}
			else if(resp =='n')
			{
				break;
			}else
			{
				printf("--ERROR: RESPUESTA ERRONEA--\n");
				break;
				
			}
			
			
		}
	}
}
